const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const gulpFont = require('gulp-font');
const watch = require('gulp-watch');
const browserSync = require('browser-sync');



gulp.task('sass', function () {
    return gulp.src('./src/styles/main.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./bild/styles'));
  });

gulp.task('img', function () {
    gulp.src('./src/img/*')
      .pipe(image())
      .pipe(gulp.dest('./bild/img'));
  });

gulp.task('font', function () {
    gulp.src('./src/fonts/*')
      .pipe(gulp.dest('./bild/fonts'));
  });

gulp.task('pages', function () {
    gulp.src('./src/pages/*')
      .pipe(gulp.dest('./bild'));
  });

gulp.task('browserSync', function () {
    browserSync ({
        server: {
            baseDir: './bild',
        }
    })
})

gulp.task ('watch', function () {
    gulp.watch ('./src/styles',['sass']);
    gulp.watch ('./src/pages',['sass']);
    gulp.watch ('./src/img',['sass']);
    gulp.watch ('./src/fonts',['sass']);
})

gulp.task ('default', ['sass', 'font', 'img', 'pages', 'browserSync', 'watch'], function (){});

gulp.task ('watch', function () {
    gulp.watch('./src/styles/main.scss',['sass']);
    gulp.watch('./src/pages/index.html',['pages']);
    gulp.watch('./src/img',['img']);
    gulp.watch ('./src/fonts',['sass']);
    gulp.watch('./src/styles/main.scss').on('change', browserSync.reload);
    gulp.watch('./src/pages/*').on('change', browserSync.reload);
})